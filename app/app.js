const app = angular.module('app', ['ngRoute', 'angular-jwt', 'ui.utils.masks', 'idf.br-filters', 'angucomplete-alt', 'ngDragDrop']);


// ROUTER SETTINGS
app.config(function ($routeProvider, $httpProvider) {
    $routeProvider.when('/home', {
        templateUrl: 'app/home.html',
        controller: 'HomeController as home'
    })
        .when('/login', {
            templateUrl: 'app/login.html',
            controller: 'LoginController as login'
        })
        .when('/pacientes', {
            templateUrl: 'app/pacientes.html',
            controller: 'PacienteController as paciente'
        })
        .when('/paciente/:id?', {
            templateUrl: 'app/paciente.html',
            controller: 'PacienteController as paciente'
        })
        .when('/funcionarios', {
            templateUrl: 'app/funcionarios.html',
            controller: 'FuncionarioController as func'
        })
        .when('/funcionario/:id?', {
            templateUrl: 'app/funcionario.html',
            controller: 'FuncionarioController as func'
        })
        .when('/visita/:id?', {
            templateUrl: 'app/visita.html',
            controller: 'VisitaController as vis'
        })
        .when('/visitas', {
            templateUrl: 'app/visitas.html',
            controller: 'VisitaController as vis'
        })
        .when('/quartos', {
            templateUrl: 'app/quartos.html',
            controller: 'QuartoController as qrt'
        })
        .when('/fila', {
            templateUrl: 'app/fila.html',
            controller: 'FilaController as fila'
        })
        .when('/diagnostico/:visitaID', {
            templateUrl: 'app/diagnostico.html',
            controller: 'DiagnosticoController as dg'
        })
        .otherwise({
            redirectTo: '/home'
        });

    $httpProvider.interceptors.push('MainInterceptor');
});

// SERVICES

app.factory('MainInterceptor', function ($log, $rootScope, $window, $timeout) {
    let service = {};

    service.request = function (config) {
        config.headers.Authorization = 'Bearer ' + $window.localStorage.getItem('sessionToken');
        $rootScope.loader = true;
        return config;
    }

    service.response = function (response) {
        $rootScope.loader = false;
        if (response.status === 201) {
            $rootScope.notify('Feito!', 'is-success')
        }
        return response;
    }

    service.responseError = function (response) {
        $rootScope.loader = false;
        if (response.status === 401) {
            $rootScope.notify('Sessão Expirada, voltando para pagina de login', 'is-danger')
            $timeout(() => {
                $rootScope.logout();
            }, 2200)
        }
        else if (response.status === 400) {
            console.log(response);
            $rootScope.showError(response.data);
        }
    }

    return service;
});

app.factory('VisitaService', function ($http, $rootScope) {
    let service = {};

    service.getLastVisits = function () {
        return $http.get($rootScope.apiURL + '/visitas')
            .then(res => {
                return res.data;
            });
    }

    return service;
});

app.factory('LoginService', function ($http, $rootScope, $window, $location, jwtHelper) {
    let service = {};

    service.auth = function (credentials) {
        return $http.post($rootScope.apiURL + '/login', credentials)
            .then(res => {
                if (res.data.auth) {
                    let decoded = jwtHelper.decodeToken(res.data.token);
                    $window.localStorage.setItem('sessionToken', res.data.token);
                    $window.localStorage.setItem('userInfo', JSON.stringify(res.data.user));
                    $location.path('/home');
                }
            });
    };

    return service;
});

app.factory('DataService', function ($http, $rootScope) {
    let service = {};

    service.new = function (entity, data) {
        return $http.post($rootScope.apiURL + `/${entity}`, data);
    }

    service.getAll = function (entity, params = {}) {
        return $http.get(`${$rootScope.apiURL}/${entity}`, {params: params});
    }

    service.getOne = function (entity, id) {
        return $http.get(`${$rootScope.apiURL}/${entity}/${id}`);
    }

    service.update = function (entity, id, data = null) {
        return $http.put(`${$rootScope.apiURL}/${entity}/${id}`, data);
    }

    service.delete = function (entity, id) {
        return $http.delete(`${$rootScope.apiURL}/${entity}/${id}`);
    }

    return service;
})

// GLOBAL FUNCTIONS AND ATTRIBUTES
app.run(function ($rootScope, $window, $location, jwtHelper) {
    $rootScope.apiURL = "http://localhost:8000/api";
    $rootScope.checkIfLogged = function () {
        if ($window.localStorage.getItem('sessionToken')) {
            return true;
        } else {
            return false;
        }
    }

    $rootScope.setTitle = function (title) {
        $rootScope.title = title;
    }

    $rootScope.logout = function () {
        $window.localStorage.clear();
        $location.path('/login');
    }

    $rootScope.getUserInfo = function () {
        try{
            return JSON.parse($window.localStorage.getItem('userInfo'))
        }catch (e) {
            $rootScope.logout();
        }
    }

    $rootScope.getUserName = function () {
        try{
            let info = JSON.parse($window.localStorage.getItem('userInfo'))
            return info.funcionario[info.funcionario.tipo].pessoa.nome;
        }catch (e) {
            $rootScope.logout();
        }
    }

    $rootScope.notify = function (message, type, duration = 2000) {
        bulmaToast.toast({
            message,
            type,
            dismissible: false,
            position: "bottom-center",
            duration: duration
        });
    }

    $rootScope.showError = function (error) {
        if (typeof error === 'object') {
            console.log(error);
            $rootScope.showErrorData = true;
            $rootScope.errorData = error;
        } else {
            $rootScope.notify(error, 'is-danger', 5000);
        }
    }

    $rootScope.checkTokenValidity = function () {
        let token = $window.localStorage.getItem('sessionToken');
        if (jwtHelper.isTokenExpired(token)) {
            return false;
        } else {
            return true;
        }
    }

    $rootScope.getUserType = function () {
        let dados = $window.localStorage.getItem('userInfo');
        dados = JSON.parse(dados);
        return dados['funcionario']['tipo'];
    }

    $rootScope.$on('$routeChangeStart', function ($event, next, current) {
        if (next['$$route']['templateUrl'] !== 'app/login.html') {
            if (!$rootScope.checkTokenValidity()) {
                $event.preventDefault();
                $rootScope.logout();
            }
        }
    });
});


// CONTROLLERS
app.controller('HomeController', function ($rootScope, DataService) {
    $rootScope.title = "Home";

    this.visitas = [];

    this.init = function () {
        DataService.getAll('visita', {limit: 10}).then(res => {
            this.visitas = res.data;
        });
    }

});

app.controller('LoginController', function ($rootScope, LoginService) {
    $rootScope.title = "Login";

    this.credentials = {};

    this.logar = function () {
        LoginService.auth(this.credentials);
    }
});

app.controller('PacienteController', function ($rootScope, DataService, $location, $routeParams) {
    $rootScope.title = 'Paciente';
    this.entity = 'paciente'
    let datepicket = null;

    let self = this;
    this.novo = {};
    this.pacientes = [];
    this.init = function () {

        if ($routeParams.id) {
            this.getOne($routeParams.id);
        }

        document.getElementsByClassName('datetimepick')
            .flatpickr({
                altInput: true,
                altFormat: "d/m/Y",
                dateFormat: "Y-m-d",
            });
    }

    this.todos = function () {
        DataService.getAll(this.entity)
            .then(response => {
                self.pacientes = response.data;
            });
    }

    this.getOne = function (id) {
        DataService.getOne(this.entity, id)
            .then(res => {
                this.novo = res.data;
                let datapick = document.querySelector('#pacienteNascimento')._flatpickr;
                datapick.setDate(moment(res.data.pessoa.data_nascimento).toDate());
            })
    }

    this.salvar = function () {

        if ($routeParams.id) {
            DataService.update(this.entity, $routeParams.id, this.novo)
                .then(res => {
                    if (res.status === 201) {
                        $location.path('/pacientes');
                    }
                });
        } else {
            DataService.new(this.entity, this.novo)
                .then(res => {
                    if (res.status === 201) {
                        $location.path('/pacientes');
                    }
                })
        }

    }

    this.excluir = function (id) {
        DataService.delete(this.entity, id)
            .then(res => {
                    this.todos();
                }
            )
    }
});


app.controller('FuncionarioController', function (DataService, $rootScope, $location, $routeParams, $timeout) {
    $rootScope.title = 'Funcionario';
    let self = this;

    this.novo = {};
    this.novo_pessoa = {};

    this.funcionarios = [];
    this.especialidades = [];

    this.helpers = {};

    this.init = function () {

        if ($routeParams.id) {
            this.buscaFuncionario($routeParams.id);
        } else {
            this.todos();
        }

        this.todasEspecialidades();

        document.getElementsByClassName('datetimepick')
            .flatpickr({
                altInput: true,
                altFormat: "d/m/Y",
                dateFormat: "Y-m-d",
            });
    }

    this.todasEspecialidades = function () {
        DataService.getAll('especialidade')
            .then(res => {
                self.especialidades = res.data;
            });
    }

    this.todos = function () {
        DataService.getAll('funcionario')
            .then(res => {
                self.funcionarios = res.data;
            });
    }

    this.buscaFuncionario = function (id) {
        DataService.getOne('funcionario', id)
            .then(res => {
                self.novo = res.data;
                let data_nascimento = document.querySelector('#pacienteNascimento')._flatpickr;
                data_nascimento.setDate(moment(res.data.funcionario[res.data.funcionario.tipo].pessoa.data_nascimento).toDate());

                let data_contratacao = document.querySelector('#funcionarioContratacao')._flatpickr;
                data_contratacao.setDate(moment(res.data.funcionario.data_contratacao).toDate());

                this.helpers.idEspecialidades = res.data.funcionario.medico.formacao.map(item => {
                    return item['id'];
                });

            });
    }

    this.salvar = function () {
        if ($routeParams.id) {
            DataService.update('funcionario', $routeParams.id, this.novo)
                .then(res => {
                    // console.log(res);
                    if (res.status === 200) {
                        $location.path('/funcionarios');
                    }
                });
        } else {
            DataService.new('funcionario', this.novo)
                .then(res => {
                    // console.log(res);
                    if (res.status === 200) {
                        $location.path('/funcionarios');
                    }
                })
        }
    }

    this.desativar = function (id) {
        DataService.delete('funcionario', id)
            .then(res => {
                if (res.status === 200) {
                    self.todos();
                }
            })
    }

    this.setExercicio = function (espc) {
        this.novo.funcionario.medico.exercicio = espc;
    }

    this.eExercicio = function (func) {
        // console.log(func, this.novo.funcionario.medico.exercicio);
        return angular.equals(func, this.novo.funcionario.medico.exercicio);
    }
});

app.controller('VisitaController', function ($rootScope, DataService, $location, $routeParams) {
    $rootScope.title = ($routeParams.id ? 'Editar Visita' : 'Visita');
    let self = this;

    this.novo = {};
    this.alta = {};
    this.filtro = {};
    this.medicos = [];
    this.visitas = [];


    this.init = function () {
        document.getElementsByClassName('datetimepick')
            .flatpickr({
                altInput: true,
                enableTime: true,
                time_24hr: true,
                altFormat: "d/m/Y H:i",
                dateFormat: "Y-m-d H:i:s",
                defaultDate: new Date(),
                closeOnSelect: true
            });
        document.getElementsByClassName('datetimepickData')
            .flatpickr({
                altInput: true,
                enableTime: false,
                time_24hr: true,
                altFormat: "d/m/Y",
                dateFormat: "Y-m-d",
                closeOnSelect: true
            });
        this.novo.data_visita = moment().format('YYYY-MM-DD HH:mm:ss')

        DataService.getAll('funcionario', {tipo: 'medico'}).then(res => {
            res.data.forEach(item => {
                self.medicos.push(item.funcionario.medico);
            });
        });

        if ($routeParams.id) {
            DataService.getOne('visita', $routeParams.id).then(res => {
                self.novo = res.data;

                let data_contratacao = document.querySelector('#dataVisita')._flatpickr;
                data_contratacao.setDate(moment(res.data.data_visita).toDate());

                $rootScope.$broadcast('angucomplete-alt:changeInput', 'paciente', res.data.paciente);

            });
        } else {
            this.filtro.data_visita = moment().format('YYYY-MM-DD');
            let data_contratacao = document.querySelector('#datetimepickData')._flatpickr;
            data_contratacao.setDate(moment().toDate());
            this.todos();
        }
    }

    this.todos = function () {
        DataService.getAll('visita', {
            paciente: this.filtro.nome,
            medico: this.filtro.medico,
            data_visita: this.filtro.data_visita
        })
            .then(res => {
                self.visitas = res.data;
            });
    }

    this.pacienteSelecionado = function (sel) {
        self.novo.paciente = sel.originalObject;
    }

    this.salvar = function () {
        if (!this.novo.id) {
            DataService.new('visita', this.novo)
                .then(res => {
                    console.log(res);
                });
        } else {
            DataService.update('visita', this.novo.id, this.novo)
                .then(res => {
                    if (res.status === 201) {
                        $location.path('/visitas');
                    }
                });
        }
    }

    this.visitaStatusClass = function (id) {
        switch (id) {
            case 1:
                return 'is-link';
                break;
            case 2:
                return 'is-danger';
            case 3:
                return 'is-warning';
            default:
                return 'is-info';
        }
    }

    this.altaModal = function (id) {
        this.alta.id = id;

        let data_alta = document.querySelector('#data_alta')._flatpickr;
        data_alta.setDate(moment().toDate());
        this.alta.data_alta = moment().format('YYYY-MM-DD HH:mm:ss');
        this.showAltaModal = true;
    }

    this.cancelarAlta = function () {
        this.alta = {};
        this.showAltaModal = false;
    }

    this.darAlta = function () {
        DataService.update('alta-visita', this.alta.id, this.alta)
            .then(res => {
                if (res.status === 201) {
                    self.cancelarAlta();
                }
            });
    }

    this.filtrar = function (event) {
        this.todos();
    }

});

app.controller('QuartoController', function (DataService, $rootScope) {
    $rootScope.title = 'Quartos';
    let self = this;
    this.filtros = {};
    this.quartos = [];
    this.andares = [];
    this.statusQuarto = [];
    this.exibirForm = false;

    this.quartoEditar = {};

    this.init = function () {
        this.todosQuartos();
        this.todosAndares();
        this.todosStatusQuarto();
    }

    this.editar = function (qrt) {
        this.exibirForm = true;
        this.quartoEditar = {};
        this.quartoEditar = qrt;
    }

    this.todosQuartos = function () {
        DataService.getAll('quarto', this.filtros)
            .then(res => {
                self.quartos = res.data;
            });
    }

    this.todosAndares = function () {
        DataService.getAll('andar')
            .then(res => {
                self.andares = res.data;
            });
    }

    this.todosStatusQuarto = function () {
        DataService.getAll('status-quarto')
            .then(res => {
                self.statusQuarto = res.data;
            });
    }

    this.getStatus = function (id) {
        let status = this.statusQuarto.filter(item => item.id === id);
        return status[0]['nome'];
    }

    this.salvar = function () {
        if (this.quartoEditar.id) {
            DataService.update('quarto', this.quartoEditar.id, this.quartoEditar)
                .then(res => {
                    if (res.status === 201) {
                        self.todosQuartos();
                        self.quartoEditar = {};
                        self.exibirForm = false;
                    }
                });
        } else {
            DataService.new('quarto', this.quartoEditar)
                .then(res => {
                    if (res.status === 201) {
                        self.todosQuartos();
                        self.quartoEditar = {};
                        self.exibirForm = false;
                    }
                });
        }
    }
})

app.controller('FilaController', function (DataService, $location, $rootScope) {
    $rootScope.title = "Fila médica";
    let self = this;
    this.filaMedica = [];


    this.init = function () {
        DataService.getAll('fila', {data: moment().format('YYYY-MM-DD')})
            .then(res => {
                self.filaMedica = res.data;
            });
    }

    this.teste = function () {
        console.log('ashdkjashkdjashdk');
    }

    this.calcTime = function (visita) {
        let agora = moment();
        let dataVisita = moment(visita['data_visita'], 'YYYY-MM-DD HH:mm:ss');
        let diff = moment.duration(agora.diff(dataVisita, 'minutes'));

        return diff + ' minutos';
    }

    this.setFicha = function (visita) {
        this.visitaFicha = angular.copy(visita);
        this.visitaFicha.data_visita = moment(this.visitaFicha.data_visita).format('DD/MM/YYYY HH:mm:ss');
    }

    this.ordernarVisita = function (event, ui, visita, index) {

        let visita_ordem = [];

        this.filaMedica[0]['visitas'].forEach((item, index) => {
            visita_ordem.push({id: item['id'], ordem: index + 1});
        });

        DataService.update('fila', 1, visita_ordem)
            .then((res) => {
                self.init();
            });

    }

});


app.controller('DiagnosticoController', function (DataService, $location, $rootScope, $routeParams) {

    this.visita = null;
    this.novo = {};
    let self = this;

    $rootScope.title = 'Diagnostico';

    this.init = function () {

        document.getElementsByClassName('datetimepick')
            .flatpickr({
                altInput: true,
                enableTime: true,
                time_24hr: true,
                altFormat: "d/m/Y H:i",
                dateFormat: "Y-m-d H:i:s",
                defaultDate: new Date(),
                closeOnSelect: true
            });


        if (!$routeParams.visitaID) {
            $location.path('/visitas');
        } else {
            DataService.getOne('visita', $routeParams.visitaID)
                .then(res => {
                    self.visita = res.data;
                    self.visita.idade = self.calcIdade(res.data.paciente.pessoa.data_nascimento);
                });
        }

        let data = document.querySelector('#dataDiagnostico')._flatpickr;
        data.setDate(moment().toDate());
        this.novo.data = moment().format('YYYY-MM-DD HH:mm:ss');

        this.novo.visita = $routeParams.visitaID;

    }

    this.calcIdade = function (nascimento) {
        let idade = moment().diff(moment(nascimento, 'YYYY-MM-DD'), 'years');
        return idade;
    }

    this.salvar = function () {
        console.log(this.novo);
    }

});